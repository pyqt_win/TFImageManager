import os

from PyQt5.QtWidgets import QFileDialog

from Image_detail_window import TFImageDetailWindow
from pyuic.MainWindow import Ui_MainWindow  # 假设 Ui_MainWindow 是由 Qt Designer 生成的类
from PyQt5 import QtGui, QtWidgets
from data.image_data_manager import TFImageDataManager
from flow_view_model import *

# home_directory = os.path.normpath(os.path.expanduser("~/"))
home_directory = os.path.expanduser("~/")
image_folder = os.path.join(home_directory, "Pictures")


class MainWindowController(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self):
        super().__init__()
        # 数据部分

        self.current_path = image_folder
        self.data_manager = TFImageDataManager()
        self.data_manager.deficit_one.connect(self._append_image)

        self.setupUi(self)  # 初始化界面
        # UI相关部分
        self.connectButtonsEvent()

        self._normal_model = TFFlowViewModel()
        self._normal_model.column = 2
        self._searching_model = TFFlowViewModel()
        self._searching_model.column = 2
        self.flowView.model = self._normal_model

        self._searching = False


        self.flowView.load_next_page.connect(self._load_next_page)
        self.flowView.clicked_item_sig.connect(self._show_image_detail_window)

        self.next_window:TFImageDetailWindow = None

    def _get_model(self):
        if self._searching:
            return self._searching_model
        else:
            return self._normal_model

    def retranslateUi(self, MainWindow):
        super().retranslateUi(MainWindow)
        self.setWindowTitle("图片管理器")
        self.search_field.setPlaceholderText("搜索图片")
        self.search_field.setClearButtonEnabled(True)

        # self.search_field.editingFinished.connect(self.path_edit_finished)
        self.search_field.textChanged.connect(self.search_text_changed)
        self.search_field.editingFinished.connect(self.searching_finished)

        self.cur_path_label.setText("当前地址: " + self.current_path)

    def connectButtonsEvent(self):
        self.pushButton.clicked.connect(self.select_directory)  # 假设 pushButton 是你的按钮控件名

    def showEvent(self, a0: QtGui.QShowEvent) -> None:
        super().showEvent(a0)
        # self.finder.find_images()
        # self.data_manager.start_update_data()
        self._load_first_page()

    def _append_image(self, image: TFImage):
        item: TFFlowViewModelItem = TFFlowViewModelItem(image)
        print("append item: ", item.image.path)
        self.flowView.model.append_item(item)

    def load_image_from_dir(self, dir_path, offset, limit, search_text=None):
        # self.finder.root_path = dir_path
        # self.data_manager.root_path = dir_path

        images = self.data_manager.get_images(dir_path, offset, limit, search_text)
        # print("images: ", len(images))
        for image in images:
            print(image.path)
        # self.show_images(images)
        return images

    def _load_next_page(self):
        offset = len(self.flowView.model.items)
        limit = self.flowView.model.page_count

        images = self.load_image_from_dir(self.current_path, offset, limit, self.search_field.text())
        self.append_images(images)

    def _load_first_page(self):
        offset = 0
        limit = self.flowView.model.page_count

        images = self.load_image_from_dir(self.current_path, offset, limit, self.search_field.text())
        self.show_images(images)

    ############显示、增、删、改图片################
    def show_images(self, images: [TFImage]):
        items = []
        for image in images:
            item: TFFlowViewModelItem = TFFlowViewModelItem(image)
            items.append(item)

        print("show_images: ", len(items))
        self.flowView.model.items = items

    def append_images(self, images: [TFImage]):
        items = []
        for image in images:
            item: TFFlowViewModelItem = TFFlowViewModelItem(image)
            items.append(item)

        self.flowView.model.append_items(items)

    # def path_edit_finished(self):
    #     dir_path = self.lineEdit.text()
    #     if not os.path.isdir(dir_path):
    #         print("path error")
    #         return
    #     self.current_path = dir_path
    #     self._load_first_page()

    def select_directory(self):
        # 打开文件夹选择对话框
        directory = QFileDialog.getExistingDirectory(self, "选择文件夹")

        # 如果用户选择了一个文件夹，则打印其路径
        if directory:
            self.current_path = directory
            self._load_first_page()
            self.cur_path_label.setText("当前地址: "+directory)

    ###搜索图片###
    def search_text_changed(self):
        print("search_text_changed:", self.search_field.text())

        searching_text = self.search_field.text()
        if len(searching_text) == 0:
            self.flowView.model = self._normal_model
        else:
            self.flowView.model = self._searching_model

        self._load_first_page()


    def searching_finished(self):
        print("sraching_finished")

    ###查看详情页###
    def _show_image_detail_window(self, item: TFFlowViewModelItem):
        self.next_window = TFImageDetailWindow()
        self.next_window.image = item.image
        self.next_window.close_signal.connect(self._closed_image_detail_window)
        self.next_window.showMaximized()

        self.setEnabled(False)

    def _closed_image_detail_window(self):
        self.next_window = None
        self.setEnabled(True)




