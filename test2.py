import threading
from PyQt5.QtCore import Qt, QSize, pyqtSignal, QThreadPool, QRunnable, QObject
from PyQt5.QtGui import QImageReader, QPixmap
import sys
from time import sleep
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout
from PyQt5.QtCore import pyqtSignal, QThread

def test_func(pixmap, text):
    print("test_func")
    print(pixmap, text)

class WorkerSignals(QObject):
    finished = pyqtSignal(QPixmap, str)
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)

class Worker(QThread):
    finished = pyqtSignal(str, str)

    def __init__(self, path, key):
        super(Worker, self).__init__()
        self.path = path
        self.key = key

    def run(self):
        sleep(3)
        self.finished.emit(self.path, self.key)

class TFImageLoaderWorker(QRunnable, QObject):
    finished = pyqtSignal(QPixmap, str)

    def __init__(self, key):
        # super().__init__()

        # 首先调用 QObject 的 __init__ 方法
        QObject.__init__(self)
        # # 然后调用 QRunnable 的 __init__ 方法（如果必要的话）
        # # QRunnable 通常不需要显式调用 __init__，除非你有特殊需求
        # QRunnable.__init__(self)
        self.key = key
    

        # 初始化你的其他属性和变量
    # def __init__(self, key, path, target_size=None):
    #     super().__init__()
    #     self.path = path
    #     self.target_size = target_size
    #     self.key = key
    #     self.signals: WorkerSignals = WorkerSignals()

    def run(self):
        sleep(1)
        self.finished.emit(QPixmap(), self.key)

class MyApp(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.worker = None
        self.pool = QThreadPool()

    def initUI(self):
        layout = QVBoxLayout()
        btn = QPushButton('点击我', self)
        btn.clicked.connect(self.test)
        layout.addWidget(btn)
        self.setLayout(layout)
        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('PyQt5 示例')
        self.show()

    def test(self):
        print('按钮被点击了！')
        path = "C:\\Users\\95/Pictures\\139-150401115324.jpg_391_219"
        worker = TFImageLoaderWorker("key_2")
        # worker = Worker("2", "1")
        self.worker = worker
        worker.finished.connect(test_func)
        # worker.start()
        self.pool.start(worker)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyApp()
    sys.exit(app.exec_())