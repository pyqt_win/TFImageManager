from PyQt5 import QtWidgets, QtGui
from PyQt5.QtCore import QSize, QPoint, Qt, pyqtSignal
from PyQt5.QtGui import QFontMetrics, QFont

from utility.view_utility import remove_all_children


class TFTagItemView(QtWidgets.QLabel):
    selected_signal = pyqtSignal(QtWidgets.QLabel)

    def __init__(self):
        super().__init__()

        self.border_radius = 4
        self._select_enable = False
        self._my_selected = False

        self.update_style()

    @property
    def select_enable(self):
        return self._select_enable

    @select_enable.setter
    def select_enable(self, value):
        self._select_enable = value

        if not self._select_enable:
            # self.selected = False
            pass
        else:
            self.update_style()

    @property
    def selected(self):
        return self._my_selected

    @selected.setter
    def selected(self, value):
        print(f'11111selected:{self._my_selected}, enable:{self.select_enable}, value:{value}, tag:{self.text()}', self)
        self._my_selected = value

        self.update_style()

        print(f'22222selected:{self._my_selected}, enable:{self.select_enable}, value:{value}, tag:{self.text()}', self)

    def update_style(self):
        self.setContentsMargins(self.border_radius, self.border_radius, self.border_radius, self.border_radius)

        # border:2px; border-style: outset
        if self._my_selected and self._select_enable:
            self.setStyleSheet(f'''color: white;background-color: rgb(150, 250, 0);
                        border-radius: {self.border_radius}px; ''')
        else:
            print(f'33333selected:{self._my_selected}, enable:{self.select_enable}, tag:{self.text()}',
                  self)
            self.setStyleSheet(f'''color: black;background-color: rgb(200, 200, 200);
                                    border-radius: {self.border_radius}px; ''')

    # def mouseReleaseEvent(self, ev: QtGui.QMouseEvent) -> None:
    #     if self._select_enable:
    #         self.selected = not self._selected
    #         self.selected.emit(self)

    def mousePressEvent(self, ev: QtGui.QMouseEvent) -> None:
        if self._select_enable:
            self.selected = not self._my_selected
            self.selected_signal.emit(self)

class TFTagView(QtWidgets.QFrame):
    selected_tags_changed = pyqtSignal(list)

    def __init__(self, *__args):
        super().__init__(*__args)
        self._tags: [str] = []
        self._tags_item_views = []

        self.edit_enable = False
        self._selected_tags = []

        self._content_height = 0
        self.min_height = 0

        # self.setStyleSheet(f'''background-color: rgb(230, 100, 50);border:4px;''')

    @property
    def tags(self):
        return self._tags

    @tags.setter
    def tags(self, tags):
        self._tags = tags
        self.layout_tags_label()
        self.update_items_view()

    @property
    def selected_tags(self):
        return self._selected_tags

    @selected_tags.setter
    def selected_tags(self, tags):
        if self._selected_tags != tags:
            self._selected_tags = tags

            self.update_items_view()

    def layout_tags_label(self):
        for child in self._tags_item_views:
            # 从父控件中移除子控件
            child.setParent(None)
        self._tags_item_views = []

        line_space = 4
        border_radius = 4
        cur_origin = QPoint(line_space, line_space)

        self._content_height = 0
        for tag in self._tags:

            item_view = TFTagItemView()
            item_view.setText(tag)
            item_view.select_enable = self.edit_enable
            if self.edit_enable:
                item_view.selected_signal.connect(self._selected_tag)
                item_view.selected = tag in self.selected_tags

            item_view.setParent(self)
            item_view.show()
            self._tags_item_views.append(item_view)

            left_width = self.width() - line_space - cur_origin.x()

            font_metrics = QFontMetrics(item_view.font())
            content_width = font_metrics.width(tag) + border_radius * 2
            content_height = font_metrics.height() + border_radius * 2

            if content_width > left_width:
                cur_origin.setY(cur_origin.y() + content_height + line_space)
                cur_origin.setX(line_space)
                # left_width = self.width() - line_space

            item_view.setGeometry(cur_origin.x(), cur_origin.y(), content_width, content_height)
            cur_origin.setX(cur_origin.x() + content_width + line_space)
            self._content_height = cur_origin.y() + content_height + line_space

        # geo = self.geometry()
        # geo.setHeight(self._content_height)
        self.updateGeometry()

    def update_items_view(self):
        for item_view in self._tags_item_views:
            item_view.select_enable = self.edit_enable
            item_view.selected = item_view.text() in self.selected_tags

    def sizeHint(self) -> QSize:
        print("sizeHint", self.edit_enable, self._content_height)
        return QSize(self.size().height(), max(self._content_height, self.min_height))

    def resizeEvent(self, a0: QtGui.QResizeEvent) -> None:
        self.layout_tags_label()

    def _selected_tag(self, item_view: TFTagItemView):
        tag = item_view.text()
        if item_view.text() not in self._selected_tags:
            self._selected_tags.append(tag)
        else:
            self._selected_tags.remove(tag)

        self.selected_tags_changed.emit(self.selected_tags)
        print("_selected_tag:", tag)
