from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import pyqtSignal, QObject

from data.image import TFImage
from data.image_loader import TFImageLoader
from flow_view_model import TFFlowViewModelItem


class TFImageLabel(QtWidgets.QLabel):

    def __init__(self, double_click_func=None, press_func=None):
        super().__init__()
        self._mouse_double_click_func = double_click_func
        self._mouse_press_func = press_func

    def mouseDoubleClickEvent(self, a0: QtGui.QMouseEvent) -> None:
        print("mouseDoubleClickEvent")
        if self._mouse_double_click_func:
            self._mouse_double_click_func()

    def mousePressEvent(self, ev: QtGui.QMouseEvent) -> None:
        print("mousePressEvent")
        if self._mouse_press_func:
            self._mouse_press_func()


class TFFlowCell(QtWidgets.QWidget):
    clicked_cell = pyqtSignal(QObject)

    def __init__(self, *args):
        super().__init__(*args)
        self._item: TFFlowViewModelItem = None
        self._label = TFImageLabel(self._double_click)
        self._label.setText("测试用文字")
        self._label.setParent(self)
        self._label.setScaledContents(True)
        self._label.setStyleSheet("\n"
                                  "background-color: rgb(100, 0, 0);")

        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        self.setLayout(layout)

        # layout.addStretch(1)
        layout.addWidget(self._label)

        self.setStyleSheet("\n""background-color: rgb(0, 100, 100);")

    @property
    def label(self):
        return self._label

    @property
    def item(self):
        return self._item

    @item.setter
    def item(self, item: TFFlowViewModelItem):
        self._item = item

        image: TFImage = item.image
        TFImageLoader().load_image_scaled(image.path, self.size(), self.loaded_set_image)

    def loaded_set_image(self, pixmap):
        self._label.setPixmap(pixmap)

    @classmethod
    def height_for_item(cls, item: TFFlowViewModelItem, width):
        image: TFImage = item.image
        height = image.size.height() * width / image.size.width()
        return height

    def resizeEvent(self, a0: QtGui.QResizeEvent) -> None:
        print(self.size(), self._label.size())

    def _double_click(self):
        print("_double_click: ", self._item.column_index, self._item.row_index)
        self.clicked_cell.emit(self)
