from PyQt5.QtCore import pyqtSignal, QObject, QRect

from data.image import TFImage


class TFFlowViewModelItem:
    def __init__(self, image: TFImage):
        self.pos = QRect(0, 0, 0, 0)
        self.width = 100
        self.column_index = 0
        self.row_index = 0
        # 0 隐藏 1 显示
        self.state = 0

        self.image: TFImage = image

    def __repr__(self):
        return f"column: {self.column_index} row:{self.row_index} image:{self.image.path}"

class TFFlowViewModel(QObject):
    items_changed = pyqtSignal()
    items_appended = pyqtSignal()

    def __init__(self, *args):
        super().__init__(*args)
        self.column = 1
        self.page_count = 20
        # self.display_range: [DisplayRange] = [DisplayRange()]
        self._items: [TFFlowViewModelItem] = []

    @property
    def items(self) -> [TFFlowViewModelItem]:
        return self._items

    @items.setter
    def items(self, items):
        self._items = items

        # 重置数据
        # self.display_range: [DisplayRange] = [DisplayRange()]
        self.items_changed.emit()

    def append_items(self, items):
        self._items.extend(items)
        self.items_appended.emit()

    def append_item(self, item):
        self._items.append(item)
        self.items_appended.emit()

    @property
    def page_index(self):
        return len(self._items)/self.page_count
