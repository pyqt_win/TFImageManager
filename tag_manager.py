import os.path
import threading


_tags_file_path = 'resources/all_tags.txt'

class TFTagManager:
    _instance = None
    _lock = threading.Lock()
    _init_flag = False

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            with cls._lock:
                if not cls._instance:
                    cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        if self.__class__._init_flag:
            return

        with self.__class__._lock:
            if not self.__class__._init_flag:

                self._tags = []
                self.read_tags()

                self.__class__._init_flag = True

    @property
    def tags(self):
        return self._tags

    @tags.setter
    def tags(self, tags_value):
        self._tags = tags_value
        self.save_tags()

    def add_tag(self, tag) -> bool:
        if tag not in self._tags:
            self._tags.append(tag)
            self.save_tags()
            return True
        else:
            return False

    def read_tags(self):
        if not os.path.exists(_tags_file_path):
            return
        with open(_tags_file_path, 'r', encoding='utf-8') as f:
            self._tags = f.read().split(',')
            f.close()

    def save_tags(self):
        tags_str = ','.join(self._tags)

        with open(_tags_file_path, 'w', encoding='utf-8') as f:
            f.write(tags_str)
            f.close()
