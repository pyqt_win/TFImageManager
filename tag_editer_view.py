from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget

from data.image import TFImage
from pyuic.tag_editer import Ui_TFTagEditer
from tag_manager import TFTagManager


class TFTagEditerView(QWidget, Ui_TFTagEditer):
    close_signal = pyqtSignal(list)

    def __init__(self, tags: [str], parent=None):
        super().__init__(parent)
        self._tags = tags
        self.setupUi(self)

    def setupUi(self, TFTagEditer):
        super().setupUi(TFTagEditer)

        self.tag_view.tags = self._tags
        self.tag_view.min_height = 40

        self.tag_select_view.edit_enable = True
        self.tag_select_view.tags = TFTagManager().tags
        self.tag_select_view.selected_tags = self._tags

        self.tag_select_view.selected_tags_changed.connect(self._selected_tags_changed)

        self.pushButton.clicked.connect(self._add_tag)
        self.pushButton_2.clicked.connect(self.close)

        # self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)
        # self.setWindowFlags(QtCore.Qt.WindowType.WindowStaysOnTopHint)

    def _add_tag(self):
        tag = self.lineEdit.text()
        if (tag is None) or len(tag) == 0:
            return
        print("add tag:", tag)
        if TFTagManager().add_tag(tag):
            self._tags.append(tag)
            self.tag_select_view.tags = TFTagManager().tags
            self.tag_select_view.selected_tags = self._tags

            self.tag_view.tags = self._tags

    def _selected_tags_changed(self):
        self.tag_view.tags = self.tag_select_view.selected_tags

    def closeEvent(self, a0) -> None:
        tags = self.tag_select_view.selected_tags
        print(tags)
        self.close_signal.emit(tags)

