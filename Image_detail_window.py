from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QMainWindow

from data.image_data_manager import TFImageDataManager
from pyuic.ImageDetail import Ui_ImageDetailWindow
from data.image import TFImage
from data.image_loader import TFImageLoader
from tag_editer_view import TFTagEditerView
from utility.view_utility import size_scale_aspected_fit


class TFImageDetailWindow(QMainWindow, Ui_ImageDetailWindow):

    close_signal = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.tag_editer_view: TFTagEditerView = None
        self.setupUi(self)

        self._image: TFImage = None

    def setupUi(self, ImageDetailWindow):
        super().setupUi(ImageDetailWindow)
        self.info_view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.edit_button.clicked.connect(self._open_edit_tag_window)

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, image: TFImage):
        if self._image != image:
            self._image = image

            self.update_info_view()

            print("load image ##")
            TFImageLoader().load_image(image.path, self._load_image_done)

    def _load_image_done(self, pixmap):
        self.image_label.setPixmap(pixmap)
        self.layout_frame()

    def resizeEvent(self, a0: QtGui.QResizeEvent) -> None:
        self.layout_frame()

    def layout_frame(self):
        size = self.size()

        info_view_min_width = 50
        info_view_max_width = 400
        info_view_width = int(size.width() * 0.2)
        if info_view_width < info_view_min_width:
            info_view_width = 0
        elif info_view_width > info_view_max_width:
            info_view_width = info_view_max_width

        label_bg_width = size.width() - info_view_width
        label_size = QtCore.QSize(label_bg_width, size.height())
        if self._image is not None:
            label_size = size_scale_aspected_fit(self._image.size, label_size)

        self.image_label.setGeometry(int((label_bg_width-label_size.width())/2.0),
                                     int((self.size().height() - label_size.height()) / 2.0),
                                     label_size.width(), label_size.height())
        self.info_view.setGeometry(label_bg_width, 0, info_view_width, size.height())

        self.verticalLayout.invalidate()
        self.verticalLayout.update()
        print(self.tag_view.size())

    def update_info_view(self):

        self.name_label.setText(f"名称： {self._image.name}")
        self.path_label.setText(f"路径： {self._image.path}")
        self.size_label.setText(f'大小： {self._image.size.width()} × {self._image.size.height()}')

        # self.tag_view.tags = ["英语考研读本", "和谐社会", "鸭嘴兽", "学习", "外出打伞好", "被理想灼烧的狗"]
        self.tag_view.tags = self._image.tags

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.close_signal.emit()

    def _open_edit_tag_window(self):
        print("open_edit_tag_windows")

        self.tag_editer_view = TFTagEditerView(self._image.tags.copy())
        self.tag_editer_view.close_signal.connect(self._closed_edit_tag_window)
        self.tag_editer_view.show()

        self.setEnabled(False)

    def _closed_edit_tag_window(self, tags: [str]):
        self._image.tags = tags
        self.tag_view.tags = tags

        TFImageDataManager().update_image_tags(self._image)

        self.tag_editer_view = None
        self.setEnabled(True)
