import sys
from PyQt5 import QtWidgets
from mainController import MainWindowController  # 假设 MainWindowController 是你的控制器类名

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindowController()  # 实例化控制器类并显示窗口
    window.show()
    sys.exit(app.exec_())