from PyQt5 import QtWidgets
from PyQt5.QtCore import QSize


def remove_all_children(parent: QtWidgets.QWidget):
    # 查找并获取所有子控件
    children = parent.findChildren(QtWidgets.QWidget)

    for child in children:
        # 从父控件中移除子控件
        child.setParent(None)


def size_scale_aspected_fit(size: QSize, target_size: QSize):
    rate_x = target_size.width()/size.width()
    rate_y = target_size.height()/size.height()

    rate = min(rate_x, rate_y)

    return QSize(int(size.width()*rate), int(size.height()*rate))
