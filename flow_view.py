from typing import Type

from PyQt5 import QtWidgets, QtGui, QtCore
from flow_view_model import *
from flow_cell import TFFlowCell


class TFFlowViewColumn:
    def __init__(self):
        self.items: [TFFlowViewModelItem] = []
        self.height = 0

        self.display_start_index = 0
        self.display_end_index = -1
        self.display_cells: [TFFlowCell] = []

    def is_displayed_item(self, item):
        for cell in self.display_cells:
            if item == cell.item:
                return True

        return False


class TFFlowView(QtWidgets.QScrollArea):
    load_next_page = pyqtSignal()
    clicked_item_sig = pyqtSignal(TFFlowViewModelItem)

    def __init__(self, *__args):
        super().__init__(*__args)
        self.line_space_v = 0
        self._refresh_distance = 30
        self.contentView = QtWidgets.QFrame()
        self._model: TFFlowViewModel = None
        self.columns: [TFFlowViewColumn] = []
        self.cell_pool = TFFlowViewCellPool(TFFlowCell)

        self.config_background()

        self._loading_next_page = False

    def config_background(self):
        # content.setText("wdhionflqwejdioqw;dniqwo;diwqjd;qwjdiqiwo;dbhnqw;oixdhqwioxhn;qw;oihjqo;dhqwdnqwhdio;hbqwxchqwxcbhqwlhioxcqwo;xhinqwxjiqwxio;qwo;xi")
        # content.setWordWrap(True)
        # self.contentView.setStyleSheet("background-color: rgb(0, 200, 200);")
        self.contentView.resize(self.size())
        self.setWidget(self.contentView)

    def changeEvent(self, a0: QtCore.QEvent) -> None:
        super().changeEvent(a0)

    def wheelEvent(self, a0: QtGui.QWheelEvent):
        super().wheelEvent(a0)

    def scrollContentsBy(self, dx: int, dy: int):
        super().scrollContentsBy(dx, dy)
        vertical_scrollbar = self.verticalScrollBar()
        offset = vertical_scrollbar.value()
        # print("##vertical offset%d" % offset)
        self.update_display_items()
        self.update_cells()

        bottom_offset = self.contentView.size().height() - offset - self.size().height()
        if bottom_offset < self._refresh_distance and (not self._loading_next_page):
            print("load_next_page emit")
            self._loading_next_page = True
            self.load_next_page.emit()

    def resizeEvent(self, a0: QtGui.QResizeEvent):
        super().resizeEvent(a0)
        # self.update()
        if len(self.model.items) > 0:
            self.update_item_pos_contentSize()
            self.update_display_items()
            self.update_cells()

    def _model_changed(self):
        print("model_changed: ", len(self.model.items))
        self.update()

    def _model_append(self):
        self._loading_next_page = False
        self.update()

    @property
    def model(self):
        return self._model

    @model.setter
    def model(self, value: TFFlowViewModel):
        if self._model == value:
            return

        #之前有数据已经显示过，清除绑定和显示的内容
        if self._model is not None:
            self._model.items_changed.disconnect(self._model_changed)
            self._model.items_appended.disconnect(self._model_append)
            #之前的cell可能留存在界面上
            for column in self.columns:
                for cell in column.display_cells:
                    cell.setParent(None)
                    cell.hide()

        self._model = value
        self.columns = [TFFlowViewColumn() for _ in range(self._model.column)]
        self._model.items_changed.connect(self._model_changed)
        self._model.items_appended.connect(self._model_append)

    def update(self) -> None:
        self.update_item_pos_contentSize()
        self.update_column_items()

        self.update_display_items()
        self.update_cells()
        super().update()

    # 第一步：计算每个item的位置
    # 第二步：计算显示的item序列
    # 第三步：要显示的添加，其他的移除
    def update_item_pos_contentSize(self):
        size = self.size()
        if self.model is None:
            size.setHeight(0)
            self.contentView.resize(size)
            return

        columns_height = [0 for _ in self.columns]
        columns_row = [0 for _ in self.columns]
        shortest_column_idx = 0
        for item in self.model.items:
            width = int(self.size().width() / self._model.column)
            height = int(TFFlowCell.height_for_item(item, width))

            item.pos = QRect(shortest_column_idx * width, columns_height[shortest_column_idx], width, height)
            item.column_index = shortest_column_idx
            # item.row_index = len(shortest_column.items)
            item.row_index = columns_row[shortest_column_idx]
            columns_row[shortest_column_idx] += 1

            columns_height[shortest_column_idx] += height + self.line_space_v
            shortest_column_idx = columns_height.index(min(columns_height))

        size.setHeight(max(columns_height))
        self.contentView.resize(size)

    def update_column_items(self):
        for column in self.columns:
            column.items = []

        for item in self._model.items:
            self.columns[item.column_index].items.append(item)

        for column in self.columns:
            column.items.sort(key=lambda it: it.row_index)
            print(f"items:{column.items}")

    def update_display_items(self):
        vertical_scrollbar = self.verticalScrollBar()
        window_start = vertical_scrollbar.value()
        window_end = window_start + self.size().height()

        c_i = 0
        for column in self.columns:
            display_range = []
            for i, item in enumerate(column.items):
                print(f"column{c_i} size: {len(column.items)} i:{i}")
                top = item.pos.top()
                bottom = item.pos.bottom()
                if top <= window_end and bottom >= window_start:
                    display_range.append(i)
            display_range.sort()

            if len(display_range) == 0:
                column.display_start_index = 0
                column.display_end_index = -1
            else:
                column.display_start_index = display_range[0]
                column.display_end_index = display_range[-1]
            # if top <= window_start <= bottom:
            #     column.display_start_index = i
            # elif top <= window_end <= bottom:
            #     column.display_end_index = i
            c_i += 1
            print(f"column:{c_i} start:{column.display_start_index} end:{column.display_end_index}")

    def update_cells(self):
        for column in self.columns:
            displayed_indexes = []
            print(f"column:{self.columns.index(column)} cells:{column.display_cells}")
            for cell in column.display_cells:
                try:
                    idx = column.items.index(cell.item, column.display_start_index, column.display_end_index + 1)
                    cell.setGeometry(cell.item.pos)
                    displayed_indexes.append(idx)
                except ValueError:
                    print(f"remove cell: {cell.item}")
                    cell.setParent(None)
                    cell.hide()
                    cell.clicked_cell.disconnect(self._clicked_cell)
                    self.cell_pool.return_one_cell(cell)
                    column.display_cells.remove(cell)

            for i in range(column.display_start_index, column.display_end_index + 1):
                if i not in displayed_indexes:
                    item = column.items[i]

                    cell: TFFlowCell = self.cell_pool.get_one_cell()
                    if cell.parent() is None:
                        cell.clicked_cell.connect(self._clicked_cell)
                    cell.setGeometry(item.pos)
                    cell.item = item
                    cell.setParent(self.contentView)
                    cell.show()
                    column.display_cells.append(cell)

    ########事件处理########
    def _clicked_cell(self, cell: TFFlowCell):
        print("_clicked_cell")
        self.clicked_item_sig.emit(cell.item)


class TFFlowViewCellPool:
    def __init__(self, cell_cls: Type[QtWidgets.QWidget]):
        self.cells = []
        self.cell_cls = cell_cls
        self.gen_cells()

    def gen_cells(self):
        for _ in range(10):
            self.cells.append(self.cell_cls())

    def get_one_cell(self):
        if len(self.cells) < 1:
            self.gen_cells()

        cell = self.cells.pop()
        return cell

    def return_one_cell(self, cell):
        self.cells.append(cell)
