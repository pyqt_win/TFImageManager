import threading
import time

from PyQt5.QtCore import Qt, QSize, pyqtSignal, QThreadPool, QRunnable, QObject
from PyQt5.QtGui import QImageReader, QPixmap

class WorkerSignals(QObject):
    finished = pyqtSignal(str, QPixmap, str)
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)

class TFImageLoaderWorker(QRunnable):
    # signal = pyqtSignal(QPixmap, str)

    def __init__(self, image_key, path, target_size=None):
        super().__init__()
        self.path = path
        self.target_size = target_size
        self.image_key = image_key
        self.worker_id = image_key+str(time.time())
        self.signals: WorkerSignals = WorkerSignals()

    def run(self):
        image_reader = QImageReader()
        image_reader.setFileName(self.path)
        image_reader.setAutoTransform(True)

        if self.target_size is not None:
            scaled_size = image_reader.size().scaled(self.target_size, Qt.KeepAspectRatio)
            image_reader.setScaledSize(scaled_size)
        pixmap = QPixmap.fromImageReader(image_reader)

        self.signals.finished.emit(self.worker_id, pixmap, self.image_key)


class TFImageLoader:
    _instance = None
    _lock = threading.Lock()
    _init_flag = False

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            with cls._lock:
                if not cls._instance:
                    cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        if self.__class__._init_flag:
            return
        # if not TFImageLoader._init_flag:
        #     with cls
        self._cache = {}
        self._callbacks = {}
        self.count = 0
        self.pool = QThreadPool()
        self.workers = {}
        TFImageLoader._init_flag = True

    def load_image_scaled(self, path, target_size, loaded_back_func):
        image_key = TFImageLoader.key_with_path_size(path, target_size)
        pixmap = self._cache.get(image_key)
        if pixmap is not None:
            loaded_back_func(pixmap)
            return

        print(image_key, self._callbacks.keys())
        worker = TFImageLoaderWorker(image_key, path, target_size)
        worker.signals.finished.connect(self._image_loaded_back)
        self.workers[worker.worker_id] = worker
        self._callbacks[worker.worker_id] = loaded_back_func

        self.pool.start(worker)

    def load_image(self, path, loaded_back_func):
        self.load_image_scaled(path, None, loaded_back_func)

    @staticmethod
    def key_with_path_size(path, size: QSize):
        if size is None:
            return f"{path}_0_0"
        return "%s_%d_%d" % (path, size.width(), size.height())

    def _image_loaded_back(self, worker_id, pixmap, image_key):
        print("###__image_loaded_back:", image_key, pixmap, self._callbacks.keys())
        self._cache[image_key] = pixmap

        if worker_id in self.workers:
            self._callbacks.pop(worker_id)(pixmap)
            self.workers.pop(worker_id)
