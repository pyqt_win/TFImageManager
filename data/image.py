from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QPixmap, QImageReader


class TFImage:

    def __init__(self, path, width=-1, height=-1):
        self.path = path
        self.name = ""
        self.modify_time = 0
        self.modify_time = 0
        self.tags = []
        self.size = QSize(0, 0)
        self.key = ""

        if width >= 0 and height >= 0:
            self.size = QSize(width, height)
        else:
            image_reader = QImageReader()
            image_reader.setFileName(path)
            self.size = image_reader.size()
