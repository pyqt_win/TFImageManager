import sqlite3


class TFDBManager:
    def __init__(self):
        self.image_conn = sqlite3.connect('image_mark.db')
        c = self.image_conn.cursor()
        c.execute('''CREATE TABLE  IF NOT EXISTS images (id INTEGER PRIMARY KEY AUTOINCREMENT, parent_path text, name text, width int, 
        height int , modify_time INTEGER, tags text, unique_key text, UNIQUE(unique_key));''')
        self.image_conn.commit()

        c.close()

    def get_image_from_dir(self, dir_path, offset=0, limit=20, search_text=None, recursive=False):
        c = self.image_conn.cursor()
        path_cond = f"parent_path = '{dir_path}'"
        if recursive:
            path_cond = f"parent_path LIKE '{dir_path}%'"

        search_cond = ""
        if search_text is not None:
            search_cond = f"AND (name LIKE '%{search_text}%' OR tags LIKE '%{search_text}%')"
        sql = f"SELECT * FROM images WHERE {path_cond} {search_cond} LIMIT {limit} OFFSET {offset};"


        print(sql)
        c.execute(sql)
        rows = c.fetchall()

        c.close()
        return rows

    # images里面每一个是元组，里面的内容对应列(path, width, height, modify_time, tags)
    def insert_images(self, images):
        cursor = self.image_conn.cursor()
        try:
            # 开始事务
            self.image_conn.begin()

            for image in images:
                insert_sql = '''INSERT OR IGNORE INTO images (parent_path, name, width, height, modify_time, tags, unique_key) 
                values (?, ?, ?, ?, ?, ?, ?);'''
                cursor.execute(insert_sql, image)

            # 提交事务
            self.image_conn.commit()
        except Exception as e:
            # 发生错误时回滚事务
            self.image_conn.rollback()
            print("An error occurred:", e)

        cursor.close()

    def check_and_insert_one(self, image):
        cursor = self.image_conn.cursor()
        insert_sql = '''INSERT OR IGNORE INTO images (parent_path, name, width, height, modify_time, tags, unique_key) 
                        values (?, ?, ?, ?, ?, ?, ?);'''
        cursor.execute(insert_sql, image)

        self.image_conn.commit()

        cursor.close()

        return cursor.rowcount

    def update_image_tags(self, tags, unique_key):
        cursor = self.image_conn.cursor()
        insert_sql = 'UPDATE images SET tags = ? where unique_key = ?'
        cursor.execute(insert_sql, (tags, unique_key))

        self.image_conn.commit()

        cursor.close()

    def __del__(self):
        self.image_conn.close()
