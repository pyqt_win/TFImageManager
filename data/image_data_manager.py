import os.path
from time import sleep

from PyQt5.QtCore import pyqtSignal, QObject, QThreadPool, QRunnable

from data.data_finder import TFDataFinder
from data.database.db_manager import TFDBManager
from data.image import TFImage

#这个manager类是一个企业，worker是流水线，每次需要去一个文件目录下抓去数据，就安排一个worker去做；
# 同时这个企业有一个销售部，它负责跟用户交互，这个销售部就没有独立出一个类了。

class WorkerSignals(QObject):
    found_one_sig = pyqtSignal(TFImage)
    found_finished_sig = pyqtSignal(str)

class TFImageFindWorker(QRunnable):
    def __init__(self, path):
        super().__init__()
        self._path = path
        self.image_finder = TFDataFinder(path)
        # self.image_finder.found_one.connect(self._found_one)

        self.signals = WorkerSignals()

    def run(self):
        self.image_finder.find_images(self._found_one)
        self.signals.found_finished_sig.emit(self._path)
        print("worker run end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

    def _found_one(self, image):
        print("worker _found_one emit1111", image.path)
        self.signals.found_one_sig.emit(image)
        print("worker _found_one emit22222", image.path)

    def __del__(self):
        print(f"delete worker:{self._path}")

class TFImageDataManager(QObject):
    update_one = pyqtSignal(TFImage)
    deficit_one = pyqtSignal(TFImage)

    def __init__(self):
        super().__init__()
        self.db_manager = TFDBManager()
        # self._root_path = root_path

        self.deficit = 0

        self._find_pool = QThreadPool()
        self._findWorkers = {}
        self._found_finished_path = []
        # if len(root_path) > 0:
            # self.image_finder = TFDataFinder(root_path)
            # self.image_finder.found_one.connect(self.image_found_one)
            # self.image_finder.find_images()

    # @property
    # def root_path(self):
    #     return self._root_path
    #
    # @root_path.setter
    # def root_path(self, value):
    #     if self._root_path != value:
    #         self._root_path = value
    #         self.image_finder.root_path = value

    ###########与硬盘交互（隐性的）###########
    ######################################

    def find_image_in_path(self, path):
        if (path in self._findWorkers) or (path in self._found_finished_path):
            return
        worker = TFImageFindWorker(path)
        worker.signals.found_one_sig.connect(self.image_found_one)
        worker.signals.found_finished_sig.connect(self.image_found_finished)
        self._findWorkers[path] = worker

        self._find_pool.start(worker)

    def image_found_one(self, image: TFImage):
        print("image_found_one: ", image.path, self.deficit)
        tags = ""
        if len(image.tags) > 0:
            tags = ','.join(image.tags)
        name = os.path.basename(image.path)
        parent_path = os.path.dirname(image.path)
        unique_key = image.path
        image_data = (parent_path, name, image.size.width(), image.size.height(),
                      image.modify_time, tags, unique_key)
        inserted_count = self.db_manager.check_and_insert_one(image_data)

        if inserted_count > 0 and self.deficit > 0:
            self.deficit_one.emit(image)
            self.deficit -= 1

    def image_found_finished(self, path):
        # if path in self._findWorkers:
            # self._findWorkers.pop(path)
        self._found_finished_path.append(path)
        print("image_found_finished: ", path)

    ###########与界面交互（显性的）###########
    ######################################
    def get_images(self, root_path, offset=0, limit=20, search_text=None, recursive=False):
        self.find_image_in_path(root_path)
        rows = self.db_manager.get_image_from_dir(root_path, offset, limit, search_text, recursive)

        images = []
        for row in rows:
            path = os.path.join(row[1], row[2])
            image = TFImage(path, row[3], row[4])
            image.name = row[2]
            image.modify_time = row[5]
            image.tags = row[6].split(',') if len(row[6]) > 0 else []
            image.key = row[7]

            images.append(image)

        self.deficit = limit - len(images)
        print("get_images: ", self.deficit)

        return images

    def update_image_tags(self, image: TFImage):
        tags = ""
        if len(image.tags) > 0:
            tags = ','.join(image.tags)

        self.db_manager.update_image_tags(tags, image.key)

