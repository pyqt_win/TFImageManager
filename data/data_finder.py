import os
from PyQt5.QtCore import pyqtSignal, QObject

from data.image import *


def file_is_image(file_path, check_header=False):
    if file_path.endswith("jpg")  or file_path.endswith("png" ) or file_path.endswith("JPG"):
        return True
    # 内部前面检验
    if check_header:
        pass
    return False


class TFDataFinder(QObject):
    finished = pyqtSignal()
    found_one = pyqtSignal(TFImage)

    def __init__(self, root_path):
        super().__init__()
        self._root_path = root_path
        self.image_files: [TFImage] = []

    @property
    def root_path(self):
        return self._root_path

    @root_path.setter
    def root_path(self, value):
        if self._root_path != value:
            self._root_path = value
            self.find_images()

    def find_images(self, found_one_back=None):
        if len(self.root_path) == 0 or (not os.path.isdir(self.root_path)):
            return

        self.image_files.clear()
        for f in os.listdir(self.root_path):
            # abs_path = os.path.normpath(os.path.join(self.root_path, f))
            abs_path = os.path.join(self.root_path, f)
            if os.path.isfile(abs_path) and file_is_image(f):
                # image = TFImage(abs_path)
                image = TFImage(abs_path)
                self.image_files.append(image)

                if found_one_back is not None:
                    found_one_back(image)
                else:
                    self.found_one.emit(image)
                print("find_image_one emit: ", image.path)

        print("find_images: ", self.image_files)
        self.finished.emit()
